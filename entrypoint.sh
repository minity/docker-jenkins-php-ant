#!/bin/sh

set -e

composer self-update

exec /usr/local/bin/jenkins.sh "$@"