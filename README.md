# Jenkins PHP environment based on [Official Jenkins Docker image](https://registry.hub.docker.com/_/jenkins/)

The Jenkins Continuous Integration and Delivery server.

This image shipped with additional tools:

* ant
* php5-cli
* php5-mysql
* php5-gd
* php5-intl
* php5-curl
* php5-xcache
* php5-xdebug
* composer
* phpunit
* pdepend
* phploc
* phpmd
* phpcpd
* php_codesniffer

# Usage

```
docker run -p 8080:8080 minity/jenkins-php-ant
```

This will store the workspace in /var/jenkins_home. All Jenkins data lives in there - including plugins and configuration.
You will probably want to make a Data Volume Container (recommended)

```
docker run --name=jenkins.data -v /var/jenkins_home busybox chmod 1000:1000 /var/jenkins_home
```

and then run a server bound to it

```
docker run -p 8080:8080 --volumes-from jenkins.data minity/jenkins-php-ant
```

For detail information see [Official Jenkins Docker image README](https://github.com/cloudbees/jenkins-ci.org-docker/tree/weekly).